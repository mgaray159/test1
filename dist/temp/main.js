'use strict';

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

require('jquery-match-height');

var _modal = require('./modal');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _jquery2.default)(function () {

    var modal = new _modal.Modal('.modal');

    if ((0, _jquery2.default)('.videoPod')) {
        (0, _jquery2.default)('.videoPod').matchHeight();
    }

    (0, _jquery2.default)('.touchevents .gallery-img').on('click', function () {
        (0, _jquery2.default)(this).closest('.gallery').find('.gallery-content').removeClass('is-clicked');
        (0, _jquery2.default)(this).next('.gallery-content').addClass('is-clicked');
    });
    (0, _jquery2.default)('.touchevents .gallery-content').on('click', function () {
        (0, _jquery2.default)(this).removeClass('is-clicked');
    });
});