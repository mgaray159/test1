'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Modal = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Modal = exports.Modal = function () {
    function Modal(el, options) {
        _classCallCheck(this, Modal);

        this.el = el;
        //Object.assign not supported ie9
        /*this.options = Object.assign({
            triggerClass: '.js-modal',
            closeClass: '.modal-close'
        }, options);*/
        this.bindEvents();
    }

    _createClass(Modal, [{
        key: 'bindEvents',
        value: function bindEvents() {
            // open modal window
            (0, _jquery2.default)('.js-modal').on('click', this.openModal.bind(this));

            // close modal window on click
            (0, _jquery2.default)(this.el).on('click', this.closeModal.bind(this));

            //close modal on escape
            (0, _jquery2.default)(window).on('keyup', this.closeModal.bind(this));
        }
    }, {
        key: 'openModal',
        value: function openModal(event) {
            event.preventDefault();
            (0, _jquery2.default)(this.el).addClass('is-visible');
            (0, _jquery2.default)('body').addClass('overflow-disabled');
        }
    }, {
        key: 'closeModal',
        value: function closeModal(event) {
            if ((0, _jquery2.default)(event.target).is('.modal-close') || (0, _jquery2.default)(event.target).is(this.el) || event.which == '27') {
                event.preventDefault();
                (0, _jquery2.default)(this.el).removeClass('is-visible');
                (0, _jquery2.default)('body').removeClass('overflow-disabled');
            }
        }
    }]);

    return Modal;
}();