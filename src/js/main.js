import $ from 'jquery';
import 'jquery-match-height'
import {Modal} from './modal';

$(function(){

    const modal = new Modal('.modal');

    if($('.videoPod')) {
        $('.videoPod').matchHeight();
    }

    $('.touchevents .gallery-img').on('click', function(){
        $(this).closest('.gallery').find('.gallery-content').removeClass('is-clicked');
        $(this).next('.gallery-content').addClass('is-clicked');

    });
    $('.touchevents .gallery-content').on('click', function(){
        $(this).removeClass('is-clicked');
    });

});

