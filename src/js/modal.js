import $ from 'jquery';


export class Modal {

    constructor(el, options) {
        this.el = el;
        //Object.assign not supported ie9
        /*this.options = Object.assign({
            triggerClass: '.js-modal',
            closeClass: '.modal-close'
        }, options);*/
        this.bindEvents();

    }

    bindEvents() {
        // open modal window
        $('.js-modal').on('click', this.openModal.bind(this));

        // close modal window on click
        $(this.el).on('click', this.closeModal.bind(this));

        //close modal on escape
        $(window).on('keyup', this.closeModal.bind(this));
    };

    openModal(event) {
        event.preventDefault();
        $(this.el).addClass('is-visible');
        $('body').addClass('overflow-disabled');
    };

    closeModal(event) {
        if($(event.target).is('.modal-close') || $(event.target).is(this.el) || event.which == '27') {
            event.preventDefault();
            $(this.el).removeClass('is-visible');
            $('body').removeClass('overflow-disabled');
        }
    };

}